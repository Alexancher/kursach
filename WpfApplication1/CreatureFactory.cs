﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication1.Library;

namespace WpfApplication1
{
    public static class CreatureFactory
    { 
        public static Creature CreateRed()
        {
            return CreateRed(new Point(0, 0));
        }
        public static Creature CreateRed(Point position)
        {
            var attacks = new List<Attack>();
            attacks.Add(Attacks.AttackFactory.CreateBash());
            return new Creature(position, Color.Red, BasicValues.Speed.Medium, BasicValues.SpeedMultiplier.Low, 1, BasicValues.HP.High, BasicValues.HPMultiplier.High, attacks);
        }

        public static Creature CreateGreen()
        {
            return CreateGreen(new Point(0, 0));
        }
        public static Creature CreateGreen(Point position)
        {
            var attacks = new List<Attack>();
            attacks.Add(Attacks.AttackFactory.CreateStab());
            return new Creature(position, Color.Green, BasicValues.Speed.High, BasicValues.SpeedMultiplier.Low, 1, BasicValues.HP.Low, BasicValues.HPMultiplier.Low, attacks);
        }

        public static Creature CreateYellow()
        {
            return CreateYellow(new Point(0, 0));
        }
        public static Creature CreateYellow(Point position)
        {
            var attacks = new List<Attack>();
            attacks.Add(Attacks.AttackFactory.CreatePunch());
            return new Creature(position, Color.Yellow, BasicValues.Speed.High, BasicValues.SpeedMultiplier.High, 1, BasicValues.HP.Low, BasicValues.HPMultiplier.Low, attacks);
        }
    }
}