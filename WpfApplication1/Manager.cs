﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication1.Library;

namespace WpfApplication1
{
    public class Manager
    {
        private List<Creature> _creatures;

        public List<Creature> Creatures { get { return _creatures; } }

        public Manager()
        {
            _creatures = new List<Creature>();
        }

        public Manager(int creatureCount) : this()
        {
            var rand = new Random();
            for (int i = 0; i < creatureCount; i++)
            {
                var color = rand.Next(2);
                switch (color)
                {
                    case 0:
                        _creatures.Add(CreatureFactory.CreateRed(new Point(rand.Next(BasicValues.ScreenSize.Width), rand.Next(BasicValues.ScreenSize.Height))));
                        break;
                    case 1:
                        _creatures.Add(CreatureFactory.CreateGreen(new Point(rand.Next(BasicValues.ScreenSize.Width), rand.Next(BasicValues.ScreenSize.Height))));
                        break;
                    case 2:
                        _creatures.Add(CreatureFactory.CreateYellow(new Point(rand.Next(BasicValues.ScreenSize.Width), rand.Next(BasicValues.ScreenSize.Height))));
                        break;
                }
            }
        }

        private void _Spawn(Color color, Point position)
        {
            Creature newCreature;
            switch (color.ToString())
            {
                case "Red":
                    newCreature = CreatureFactory.CreateRed(position);
                    break;
                case "Green":
                    newCreature = CreatureFactory.CreateGreen(position);
                    break;
                case "Yellow":
                    newCreature = CreatureFactory.CreateYellow(position);
                    break;
                default:
                    return;
            }
            _creatures.Add(newCreature);
        }

        private void _DeleteCreature(Creature creature)
        {
            _creatures.Remove(creature);
        }

        private int _DistanceBetween(Creature first, Creature second)
        {
            return (int)Math.Sqrt(Math.Pow(first.Position.X - second.Position.X, 2) + Math.Pow(first.Position.Y - second.Position.Y, 2));
        }

        private Creature _ClosestTo(Creature creature)
        {
            Creature closest = null;
            var minDistance = int.MaxValue;
            foreach (var target in Creatures)
            {
                if (Creatures.IndexOf(creature) != Creatures.IndexOf(target))
                {
                    if (_DistanceBetween(creature, target) < minDistance)
                    {
                        minDistance = _DistanceBetween(creature, target);
                        closest = target;
                    }
                }
            }
            return closest;
        }

        private Attack _BestAttack(Creature creature)
        {
            Attack bestAttack = null;
            var maxDamage = 0;
            foreach (var attack in creature.Attacks)
            {
                if (attack.Damage * attack.AttackSpeed > maxDamage)
                {
                    maxDamage = attack.Damage * attack.AttackSpeed;
                    bestAttack = attack;
                }
            }
            return bestAttack;
        }

        private Directions.Direction _GetDirection(Creature creature, Creature target)
        {
            if (Math.Abs(creature.Position.X - target.Position.X) > Math.Abs(creature.Position.Y - target.Position.Y))
            {
                if (creature.Position.X > target.Position.X)
                    return Directions.Direction.Left;
                else
                    return Directions.Direction.Rigth;
            }
            else
            {
                if (creature.Position.Y > target.Position.Y)
                    return Directions.Direction.Up;
                else
                    return Directions.Direction.Down;
            }
        }

        public void Start()
        {
            while (Creatures.Count > 1)
            {
                foreach (var creature in Creatures)
                {
                    if (!creature.IsDead)
                    {
                        creature.ResetSpeed();
                        var target = _ClosestTo(creature);
                        for (int i = 0; i < creature.Speed; i++)
                        {
                            if (_DistanceBetween(creature, target) == 1)
                            {
                                if (!target.IsDead)
                                    creature.Attack(target, _BestAttack(creature));
                                else
                                {
                                    creature.Consume(target);
                                    _DeleteCreature(target);
                                }
                                break;
                            }
                            else
                            {
                                creature.Move(_GetDirection(creature, target));
                            }
                        }
                    }
                }
            }
        }
    }
}