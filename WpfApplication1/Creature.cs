﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication1.Library;

namespace WpfApplication1
{
    public class Creature
    {
        private Point _position;
        private Color _color;
        private int _speed;
        private int _reamaingSpeed;
        private double _speedMultiplier;
        private int _level;
        private int _size;
        private int _hp;
        private int _maxHP;
        private int _hpMultiplier;
        private List<Attack> _attacks;

        public Point Position { get { return _position; } }
        public Color Color { get { return _color; } }
        public int Speed { get { return _speed; } }
        public int ReaminingSpeed { get { return _reamaingSpeed; } }
        public int Level {  get { return _level; } }
        public int Size { get { return _size; } }
        public int HP { get { return _hp; } }
        public bool IsDead { get { return _hp <= 0 ? true : false; } }
        public List<Attack> Attacks { get { return _attacks; } }

        public Creature()
        {
            _position = new Point(0, 0);
            _color = Color.Transparent;
            _speed = 0;
            _reamaingSpeed = 0;
            _speedMultiplier = 0;
            _level = 0;
            _size = 0;
            _hp = 0;
            _hpMultiplier = 0;
            _maxHP = 0;
            _attacks = new List<Attack>();
        }

        internal Creature(Point position, Color color, int speed, double speedMultiplier, int level, int maxHP, int hpMultiplier, List<Attack> attacks)
        {
            _position = position;
            _color = color;
            _speed = speed + (int)(level * speedMultiplier);
            _reamaingSpeed = _speed;
            _speedMultiplier = speedMultiplier;
            _level = level;
            _size = 1 + _level / 10;
            _hp = maxHP + level * hpMultiplier;
            _hpMultiplier = hpMultiplier;
            _maxHP = maxHP + level * hpMultiplier;
            _attacks = attacks;
        }

        public void LevelUp()
        {
            _level++;
            _speed += (int)(_level * _speedMultiplier);
            _size = 1 + _level / 10;
            _hp += _level * _hpMultiplier;
            _maxHP += _level * _hpMultiplier;
            foreach (var attack in Attacks)
                attack.LevelUp(_level);
        }

        public void Move(Directions.Direction direction)
        {
            if (_reamaingSpeed <= 0)
                return;
            switch (direction)
            {
                case Directions.Direction.Up:
                    _position.Y--;
                    break;
                case Directions.Direction.Rigth:
                    _position.X++;
                    break;
                case Directions.Direction.Down:
                    _position.Y++;
                    break;
                case Directions.Direction.Left:
                    _position.X--;
                    break;
            }
            _reamaingSpeed--;

            if (_position.X > BasicValues.ScreenSize.Width)
                _position.X = 0;
            if (_position.Y > BasicValues.ScreenSize.Height)
                _position.Y = 0;
            if (_position.X < 0)
                _position.X = BasicValues.ScreenSize.Width;
            if (_position.Y < 0)
                _position.Y = BasicValues.ScreenSize.Height;
        }

        public void ResetSpeed()
        {
            _reamaingSpeed = _speed;
        }

        public void TakeDamage(int damage)
        {
            _hp -= damage;
        }

        public void Attack(Creature target, Attack attack)
        {
            attack.Use(target);
        }

        public void Consume(Creature target)
        {
            for (var i = 0; i < target.Level; i++)
                LevelUp();
            foreach (var attack in target.Attacks)
                Attacks.Add(attack);
            target = new Creature();
        }
    }
}