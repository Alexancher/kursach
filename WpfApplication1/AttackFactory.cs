﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication1.Library;

namespace WpfApplication1.Attacks
{
    public static class AttackFactory
    {
        public static Attack CreateBash()
        {
            return CreateBash(1);
        }
        public static Attack CreateBash(int level)
        {
            return new Attack(BasicValues.Damage.Medium, level, BasicValues.DamageMultiplier.Low, BasicValues.AttackSpeed.Low);
        }

        public static Attack CreateStab()
        {
            return CreateStab(1);
        }
        public static Attack CreateStab(int level)
        {
            return new Attack(BasicValues.Damage.High, level, BasicValues.DamageMultiplier.High, BasicValues.AttackSpeed.Medium);
        }

        public static Attack CreatePunch()
        {
            return CreatePunch(1);
        }
        public static Attack CreatePunch(int level)
        {
            return new Attack(BasicValues.Damage.Low, level, BasicValues.DamageMultiplier.Low, BasicValues.AttackSpeed.High);
        }
    }
}