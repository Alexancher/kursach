﻿using RealScience.Library;

namespace RealScience
{
    public static class AttackFactory
    {
        public static Attack CreateBash()
        {
            return CreateBash(1);
        }
        public static Attack CreateBash(int level)
        {
            return new Attack(BasicValues.Damage.Medium, level, BasicValues.DamageMultiplier.Low, BasicValues.AttackSpeed.Low);
        }

        public static Attack CreateStab()
        {
            return CreateStab(1);
        }
        public static Attack CreateStab(int level)
        {
            return new Attack(BasicValues.Damage.High, level, BasicValues.DamageMultiplier.High, BasicValues.AttackSpeed.Medium);
        }

        public static Attack CreatePunch()
        {
            return CreatePunch(1);
        }
        public static Attack CreatePunch(int level)
        {
            return new Attack(BasicValues.Damage.Low, level, BasicValues.DamageMultiplier.Low, BasicValues.AttackSpeed.High);
        }

        public static Attack CreateChillTouch()
        {
            return CreateChillTouch(1);
        }
        public static Attack CreateChillTouch(int level)
        {
            return new FreezingAttack(BasicValues.Damage.Medium, level, BasicValues.DamageMultiplier.Low, BasicValues.AttackSpeed.Low);
        }
    }
}