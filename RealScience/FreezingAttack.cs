﻿namespace RealScience
{
    public class FreezingAttack : Attack
    {
        public FreezingAttack(int baseDamage, int level, int damageMultiplier, int attackSpeed) : base(baseDamage, level, damageMultiplier, attackSpeed) { }

        public override void Use(Creature target)
        {
            base.Use(target);
            target.Slow();
        }
    }
}
