﻿namespace RealScience.Library
{
    public static class BasicValues
    {
        public struct Speed
        {
            public const int VeryLow = 1;
            public const int Low = 2;
            public const int Medium = 3;
            public const int High = 4;
            public const int VeryHigh = 5;
        }
        public struct SpeedMultiplier
        {
            public const double Low = 0;
            public const double High = 0.5;
        }
        public struct HP
        {
            public const int VeryLow = 10;
            public const int Low = 12;
            public const int Medium = 15;
            public const int High = 17;
            public const int VeryHigh = 20;
        }
        public struct HPMultiplier
        {
            public const int Low = 2;
            public const int High = 3;
        }
        public struct Damage
        {
            public const int Low = 3;
            public const int Medium = 5;
            public const int High = 7;
        }
        public struct DamageMultiplier
        {
            public const int Low = 2;
            public const int High = 3;
        }
        public struct AttackSpeed
        {
            public const int Low = 1;
            public const int Medium = 2;
            public const int High = 3;
        }
        public struct ScreenSize
        {
            public const int Width = 1920;
            public const int Height = 1080;
        }
        public const int CellSize = 40;
        public struct FieldSize
        {
            public const int Width = (int)(ScreenSize.Width / CellSize);
            public const int Height = (int)(ScreenSize.Height / CellSize);
        }
    }
}