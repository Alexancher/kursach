﻿using RealScience.Library;
using System.Collections.Generic;
using System.Drawing;

namespace RealScience
{
    public static class CreatureFactory
    {
        public static Creature CreateRed()
        {
            return CreateRed(new Point(0, 0));
        }
        public static Creature CreateRed(Point position)
        {
            var attacks = new List<Attack>();
            attacks.Add(AttackFactory.CreateBash());
            return new Creature(position, Color.Red, BasicValues.Speed.Medium, BasicValues.SpeedMultiplier.Low, 1, BasicValues.HP.High, BasicValues.HPMultiplier.High, attacks);
        }

        public static Creature CreateGreen()
        {
            return CreateGreen(new Point(0, 0));
        }
        public static Creature CreateGreen(Point position)
        {
            var attacks = new List<Attack>();
            attacks.Add(AttackFactory.CreateStab());
            return new Creature(position, Color.Green, BasicValues.Speed.High, BasicValues.SpeedMultiplier.Low, 1, BasicValues.HP.Low, BasicValues.HPMultiplier.Low, attacks);
        }

        public static Creature CreateYellow()
        {
            return CreateYellow(new Point(0, 0));
        }
        public static Creature CreateYellow(Point position)
        {
            var attacks = new List<Attack>();
            attacks.Add(AttackFactory.CreatePunch());
            return new Creature(position, Color.Yellow, BasicValues.Speed.High, BasicValues.SpeedMultiplier.High, 1, BasicValues.HP.Low, BasicValues.HPMultiplier.Low, attacks);
        }
        public static Creature CreateBlue()
        {
            return CreateBlue(new Point(0, 0));
        }
        public static Creature CreateBlue(Point position)
        {
            var attacks = new List<Attack>();
            attacks.Add(AttackFactory.CreateChillTouch());
            return new Creature(position, Color.LightBlue, BasicValues.Speed.Medium, BasicValues.SpeedMultiplier.Low, 1, BasicValues.HP.Medium, BasicValues.HPMultiplier.Low, attacks);
        }
    }
}