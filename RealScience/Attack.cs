﻿namespace RealScience
{
    public class Attack
    {
        private int _damage;
        private int _damageMultiplier;
        private int _attackSpeed;
        public int Damage { get { return _damage; } }
        public int AttackSpeed { get { return _attackSpeed; } }
        internal Attack(int baseDamage, int level, int damageMultiplier, int attackSpeed)
        {
            _damageMultiplier = damageMultiplier;
            _damage = baseDamage + level * _damageMultiplier;
            _attackSpeed = attackSpeed;
        }
        public void LevelUp(int level)
        {
            _damage += level * _damageMultiplier;
        }
        public virtual void Use(Creature target)
        {
            for (var i = 0; i < _attackSpeed; i++)
                target.TakeDamage(_damage);
        }
    }
}