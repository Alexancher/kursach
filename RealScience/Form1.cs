﻿using RealScience.Library;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace RealScience
{
    public partial class Form1 : Form
    {
        private Manager _manager = new Manager(42);
        private Graphics g;

        public Form1()
        {
            InitializeComponent();
            Size = new Size(BasicValues.ScreenSize.Width, BasicValues.ScreenSize.Height);
            g = CreateGraphics();
        }

        private void _ShowField()
        {
            var pen = new Pen(Color.LightGray, 2F);
            for (int i = 0; i < BasicValues.ScreenSize.Height / BasicValues.CellSize; i++)
                g.DrawLine(pen, 0, BasicValues.CellSize * i, BasicValues.ScreenSize.Width, BasicValues.CellSize * i);
            for (int i = 0; i < BasicValues.ScreenSize.Width / BasicValues.CellSize; i++)
                g.DrawLine(pen, BasicValues.CellSize * i, 0, BasicValues.CellSize * i, BasicValues.ScreenSize.Height);
        }

        private void _ShowCreatures()
        {
            var textBrush = new SolidBrush(Color.Black);
            foreach (var creature in _manager.Creatures)
            {
                var brush = new SolidBrush(creature.Color);
                var cell = new Rectangle(creature.Position.X * BasicValues.CellSize, creature.Position.Y * BasicValues.CellSize, BasicValues.CellSize, BasicValues.CellSize);
                g.FillEllipse(brush, cell);
                var font = new Font(DefaultFont, FontStyle.Regular);
                g.DrawString("lvl " + creature.Level, font, textBrush, creature.Position.X * BasicValues.CellSize, creature.Position.Y * BasicValues.CellSize);
                g.DrawString(creature.HP + "/" + creature.MaxHP, font, textBrush, creature.Position.X * BasicValues.CellSize, creature.Position.Y * BasicValues.CellSize + BasicValues.CellSize / 2);
            }
        }

        private void _Start()
        {
            while (_manager.Creatures.Count > 1)
            {
                Thread.Sleep(1000);
                g.Clear(Color.White);
                _manager.TakeTurn();
                _ShowField();
                _ShowCreatures();
            }
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            _ShowField();
            _ShowCreatures();
            _Start();
        }
    }
}